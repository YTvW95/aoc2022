import sys
import fileinput
import time
import re
if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def split(string, split_len):
    regex = r'(.{%s})' % split_len
    return [x for x in re.split(regex, string) if x]

maplines = []
instructionlines = []
columns = []
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    if cleanLine == '':
      continue
    elif cleanLine[1] == '1':
      columns=  cleanLine.split()
    elif cleanLine[0] == '['  or cleanLine[0] == ' ':
      maplines.append(cleanLine)
    else:
      instructionlines.append(cleanLine.replace('move ','').replace('from ','').replace('to ','').split())

map= {}
for column in range(0 , len(columns)) :
  map[str(column)] =[]
  for row in maplines:
    elements = split(row,4)
    element = elements[column].strip().strip("[]")
    if element != '':
      map[str(column)].append(element)

for line in range(0,len(instructionlines)):
  instruction = instructionlines[line]
  for i in range(0,int(instruction[0])):
    map[str(int(instruction[2])-1)].insert(0,map[str(int(instruction[1])-1)].pop(0))

result =[]
for i in range(0,len(columns)):
  result.append(map[str(i)].pop(0))
print("result: ",''.join(result))
print("--- %s seconds ---" % (time.time() - startTime))