import sys
import fileinput
import time
import re

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def split(string, split_len):
    regex = r'(.{%s})' % split_len
    return [x for x in re.split(regex, string) if x]

def isVisible(hash,x,y):
  xLVisible = True
  xRVisible = True
  yUVisible = True
  yDVisible = True
  key = str(x)+'-'+str(y)
  height = hash[(x,y)]
  xx=0
  while xx < maxX:
    testheight =hash[(xx,y)]
    if xx < x:
      if testheight >= height:
        xLVisible = False
        xx = x
    if xx > x:
      if testheight >= height:
        xRVisible = False
        break
    xx+=1
  yy=0
  while yy < maxY:
    testheight =hash[(x,yy)]
    if yy < y:
      if testheight >= height:
        yUVisible = False
        yy = y
    if yy > y:
      if testheight >= height:
        yDVisible = False
        break
    yy+=1
  if xLVisible == True or xRVisible == True or yUVisible == True or yDVisible == True:
    return True
  else:
    return False

map = [[]]
maphash={}
startTime = time.time()
lineNr=0
maxX = 0
maxY = 0
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    for i in range(0,len(cleanLine)):
      maphash[(i,lineNr)] = int(cleanLine[i])
    lineNr+=1
    map.append([])
maxX = len(cleanLine) 
maxY = lineNr 
# print(maphash)
count = 0 + ((maxY) * 2) + (((maxX) * 2)-4)
for y in range(1,maxY -1):
  for x in range(1,maxX -1):
    res = isVisible(maphash,x,y)
    if res == True:
      count+=1
print('result: ',count)
print("--- %s seconds ---" % (time.time() - startTime))