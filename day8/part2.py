import sys
import fileinput
import time
import re

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def split(string, split_len):
    regex = r'(.{%s})' % split_len
    return [x for x in re.split(regex, string) if x]

def isVisible(hash,x,y):
  # print('----------------------------')
  xLVisible = True
  xRVisible = True
  yUVisible = True
  yDVisible = True
  key = str(x)+'-'+str(y)
  
  height = hash[key]
  # print('point to check: ',key,height)
  for xx in range(0,maxX+1):
    key = str(xx)+'-'+str(y)
    # print(key,hash[key])
    # print(hash[key] >= height)
    if xx < x:
      if hash[key] >= height:
        # print('xLVisible: False: ',hash[key])
        xLVisible = False
    if xx > x:
      if hash[key] >= height:
        # print('xRVisible: False: ',hash[key])
        xRVisible = False
  for yy in range(0,maxY+1):
    # print(key,hash[key])
    # print(hash[key] >= height)
    key = str(x)+'-'+str(yy)
    if yy < y:
      if hash[key] >= height:
        # print('yUVisible: False: ',hash[key])
        yUVisible = False
    if yy > y:
      if hash[key] >= height:
        # print('yDVisible: False: ',hash[key])
        yDVisible = False
  # print('xLVisible: ',xLVisible)
  # print('xRVisible: ',xRVisible)
  # print('yUVisible: ',yUVisible)
  # print('yDVisible: ',yDVisible)

  # print('----------------------------')

  if xLVisible == True or xRVisible == True or yUVisible == True or yDVisible == True:
    # print('visible')
    return True
  else:
    # print('invisible')
    return False

map = [[]]
maphash={}
startTime = time.time()
lineNr=0
maxX = 0
maxY = 0
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    for i in range(0,len(cleanLine)):
      map[lineNr].append(int(cleanLine[i]))
      maphash[str(i)+'-'+str(lineNr)] = int(cleanLine[i])
    # map[lineNr]=split(cleanLine,1)
    lineNr+=1
    map.append([])
    print(cleanLine)
maxX = len(cleanLine) -1
maxY = lineNr -1
# print(maphash)
# print(maxX,maxY)

count = 0 + ((maxY+1) * 2) + (((maxX+1) * 2)-4)
for y in range(1,maxY ):
  for x in range(1,maxX ):
    key= str(x) +'-'+ str(y)
    # print(key)
    res = isVisible(maphash,x,y)
    if res == True:
      count+=1
    # print(key,maphash[key],res)
    # print(maphash[key])
print('result: ',count)
print("--- %s seconds ---" % (time.time() - startTime))