import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"
LIMIT = 100000

def getSize(filesystem,path):
  done= False
  tempSize=0
  start='/'
  keysInPath = filesystem[path].keys()
  for key in keysInPath:

  # if len(keysInPath) >=2:
    print(key)
    tempSize += getSize(filesystem,path+key)
  #   compresFileSystem(filesystem)
  if len(keysInPath) <=1:
    return size
  # else:
  #   return compresFileSystem(filesystem)
input = []
uniqueDirs = 1
filesystem={'/':{}}
startTime = time.time()
isOutput=False
currentDir = ''
fullPath =''
tempSize =0
lineNr =0
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    input.append(cleanLine)
    lineNr +=1
    if cleanLine[0] == '$' and isOutput == True:
      isOutput = False
      filesystem[fullPath]['size'] = tempSize
      tempSize = 0
      # print('[',lineNr,']',filesystem)
      # print('[',lineNr,']','done reading',cleanLine)
    elif cleanLine == '$ ls':
      isOutput=True
      # print('[',lineNr,']','start reading',cleanLine)
      # print('[',lineNr,']','listing files')
    elif isOutput == True:
      # print(cleanLine)
      if cleanLine[0:3] == 'dir':
        uniqueDirs +=1
      # print(cleanLine[4:len(cleanLine)])
        filesystem[fullPath][cleanLine[4:len(cleanLine)]]={}
      # print(filesystem)
      else:
        size = int(cleanLine.split(' ')[0])
        # print('size: ',size)
        tempSize +=size
    # print(fullPath)
    # print(cleanLine)
    if cleanLine[0:5] == '$ cd ':
      # print('[',lineNr,']','switching dirs')
      if cleanLine[5:len(cleanLine)] == '/':
        # print('switch to root')
        fullPath = '/'
      elif cleanLine[5:len(cleanLine)] != '..':
        # print('[',lineNr,']','deeper')
        if currentDir == '/':
          fullPath = fullPath + cleanLine[5:len(cleanLine)]
        else:
          fullPath = fullPath +'/'+ cleanLine[5:len(cleanLine)]

        filesystem[fullPath] = {}
        # print(fullPath)
      elif cleanLine[5:len(cleanLine)] == '..':
        # print('[',lineNr,']','back')
        print(currentDir)
        print(fullPath)
        print(fullPath.split('/'))
        fullPath = fullPath.replace('/'+currentDir,'')
        currentDir = fullPath.split('/').pop()
        if currentDir == '':
          currentDir = '/'
          fullPath = '/'
        print('currentDir after change: ',currentDir)
        print('fullPath after change: ',fullPath)
      if cleanLine[5:len(cleanLine)] != '..':
        print('new dir is not ...')
        currentDir = cleanLine[5:len(cleanLine)]
    print('currentDir after process: ',currentDir)
    print('fullPath after process: ',fullPath)
isOutput = False
filesystem[fullPath]['size'] = tempSize
tempSize = 0
    
print(filesystem)
print(uniqueDirs)
print(input)

for key in filesystem['/'].keys():
  if key != 'size':
    print(filesystem['/'+key])
    for key2 in filesystem['/'+key].keys():
      if key2 != 'size':
        print(filesystem['/'+key+'/'+key2])

# compresFileSystem(filesystem,'/')

print("--- %s seconds ---" % (time.time() - startTime))