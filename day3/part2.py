import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def getScore(char:str):
  if char.isupper():
    return ord(char)-64+26
  else:
    return ord(char)-96

sum = 0
lineCount =0
lines =[]
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    lineCount +=1
    if lineCount==1:
      lines.append(cleanLine)
    elif lineCount==2:
      lines.append(cleanLine)
    elif lineCount==3:
      lines.append(cleanLine)
      lines.sort(key=len,reverse=True)
      for char in lines[0]:
        found1 = lines[1].find(char)
        found2 = lines[2].find(char)
        if(found1 !=-1 and found2 != -1):
          sum += getScore(char)
          break;
      lines=[]
      lineCount = 0

print('result: ', sum)
print("--- %s seconds ---" % (time.time() - startTime))