import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"
  
def getScore(char:str):
  if char.isupper():
    return ord(char)-64+26
  else:
    return ord(char)-96
sum = 0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    firstPart = cleanLine[slice(0,int(len(cleanLine)/2))]
    secondPart = cleanLine[slice(int(len(cleanLine)/2),len(cleanLine))]
    for char in firstPart:
      res = secondPart.find(char)
      if res != -1:
        sum+=getScore(char)
        break
print('result: ', sum)
print("--- %s seconds ---" % (time.time() - startTime))