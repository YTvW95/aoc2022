import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def  getScore(plaBed):
  if plaBed == 'A':
    return 1
  elif plaBed == 'B':
    return 2
  elif plaBed == 'X':
    return 1
  elif plaBed == 'Y':
    return 2
  else:
    return 3

score = 0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    elements = cleanLine.split(' ')
    score1 = getScore(elements[0])
    score2 = getScore(elements[1])
    # score += score2
    if elements[1]=="X":
      if elements[0]=="A":
        score += getScore('C')
      elif elements[0]=="B":
        score += getScore('A')
      elif  elements[0]=="C":
        score += getScore('B')
    elif elements[1]=="Y":
      score += getScore(elements[0])
      score += 3
    elif  elements[1]=="Z":
      score += 6
      if elements[0]=="A":
        score += getScore('B')
      elif elements[0]=="B":
        score += getScore('C')
      elif  elements[0]=="C":
        score += getScore('A')

print('score: ',score )
print("--- %s seconds ---" % (time.time() - startTime))