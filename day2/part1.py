import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def  getScore(form):
  if form == 'A':
    return 1
  elif form == 'B':
    return 2
  elif form == 'X':
    return 1
  elif form == 'Y':
    return 2
  else:
    return 3

score = 0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    elements = cleanLine.split(' ')
    score2 = getScore(elements[1])
    score += score2
    if elements[1]=="X":
      if elements[0]=="A":
        score += 3
      elif  elements[0]=="C":
        score += 6
    elif elements[1]=="Y":
      if elements[0]=="A":
        score += 6
      elif elements[0]=="B":
        score += 3
    elif  elements[1]=="Z":
      if elements[0]=="B":
        score += 6
      elif  elements[0]=="C":
        score += 3

print('score: ',score )
print("--- %s seconds ---" % (time.time() - startTime))