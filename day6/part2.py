import sys
import fileinput
import time
from collections import Counter

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

cleanline =''
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
  cleanLine = line.strip("\n")
result = 0
for i in range(14,len(cleanLine)):
  res = Counter(cleanLine[slice(i-14,i)]).keys()
  if len(res) == 14:
    result = i
    break

print('result: ', result)
print("--- %s seconds ---" % (time.time() - startTime))