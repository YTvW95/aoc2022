import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

largestCount = [0]
temp =0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    if cleanLine == '' :
      largestCount.insert(0,temp )
      temp = 0
    else:
       temp += int(cleanLine)
largestCount.insert(0,temp )
temp = 0

largestCount.sort(reverse=True)

print(largestCount[0]+largestCount[1]+largestCount[2])
print("--- %s seconds ---" % (time.time() - startTime))