import sys
import fileinput
import time

print(sys.argv)
if len(sys.argv) >1:
  fileName = sys.argv[1]
else:
  fileName = "input"

largestCount = 0
temp =0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
   
    if cleanLine == '' :
      if temp >= largestCount:
        largestCount = temp 
   
        # print(largestCount)
      temp = 0
    else:
       temp += int(cleanLine)
    # print(cleanLine)

print(largestCount)
print("--- %s seconds ---" % (time.time() - startTime))