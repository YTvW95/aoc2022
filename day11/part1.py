import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

monkeys={}
monkeyLines=[]
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
   
    if cleanLine == '':
      print('process lines')
      monkey ={'items':[]}
      monkey['name'] =int(monkeyLines.pop(0).split()[1].strip(':'))
      items= str(monkeyLines.pop(0)).split(':')[1].split(',')
      for item in items:
        monkey['items'].append(int(item))
      operationLine =monkeyLines.pop(0).split('=')[1].split()
      monkey['operationType']= operationLine[1]
      monkey['operationValue']=operationLine[2]
      monkey['testValue']=int(monkeyLines.pop(0).split().pop())
      monkey['trueResult'] = int(monkeyLines.pop(0).split().pop())
      monkey['falseResult'] =int( monkeyLines.pop(0).split().pop())
      # monkey['items']= str(monkeyLines.pop(0)).split(':')[1].split(',')
      monkeys[monkey['name']]=monkey
      # monkey['items']= str(monkeyLines.pop(0)).split(':')[1].split(',')
      monkeyLines = []
    else:
      monkeyLines.append(cleanLine)

monkey ={'items':[]}
monkey['name'] =int(monkeyLines.pop(0).split()[1].strip(':'))
items= str(monkeyLines.pop(0)).split(':')[1].split(',')
for item in items:
  monkey['items'].append(int(item))
operationLine =monkeyLines.pop(0).split('=')[1].split()
monkey['operationType']= operationLine[1]
monkey['operationValue']=operationLine[2]
monkey['testValue']=int(monkeyLines.pop(0).split().pop())
monkey['trueResult'] = int(monkeyLines.pop(0).split().pop())
monkey['falseResult'] =int( monkeyLines.pop(0).split().pop())
# monkey['items']= str(monkeyLines.pop(0)).split(':')[1].split(',')
monkeys[monkey['name']]=monkey

print(monkeys)

print("--- %s seconds ---" % (time.time() - startTime))