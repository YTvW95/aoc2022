import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

cycles = [20,60,100,140,180,220]
X =1
cycle=1
sum = 0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
  cleanLine = line.strip("\n")
  cyclesToAdd=0
  if cleanLine != 'noop':
    cyclesToAdd=2
  else:
   cyclesToAdd=1
  
  while cyclesToAdd >0:
    cyclesToAdd -=1
    cycle += 1
    if cycle in cycles:
      sum += cycle*X
    if cyclesToAdd ==1 :
      X += int(cleanLine.split()[1])

print('result: ',sum)
print("--- %s seconds ---" % (time.time() - startTime))