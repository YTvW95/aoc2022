import sys
import fileinput
import time
import re
if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

def split(string, split_len):
    regex = r'(.{%s})' % split_len
    return [x for x in re.split(regex, string) if x]
screen=''

def setPixel(screen,X,cycle):
  pixelPos = (cycle %40)-1
  
  if X == pixelPos :
    screen += '#'
  elif X-1 == pixelPos:
    screen += '#'
  elif X+1 == pixelPos:
    screen += '#'
  else:
    screen += ' '
  return screen

X =1
cycle=1
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
  cleanLine = line.strip("\n")
  cyclesToAdd=0
  updateX = False
  if cleanLine != 'noop':
    cyclesToAdd=2
    updateX = True
  else:
   cyclesToAdd=1
  
  while cyclesToAdd >0:
    screen = setPixel(screen,X,cycle)
    cyclesToAdd -=1
    cycle += 1
    if cyclesToAdd ==0 and updateX :
      updateX = False
      X += int(cleanLine.split()[1])

lines = split(screen,40)
for line in lines:
  print(line)
# print('result: ',sum)
print("--- %s seconds ---" % (time.time() - startTime))