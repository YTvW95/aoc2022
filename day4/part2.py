import sys
import fileinput
import time

if len(sys.argv) >=2:
  fileName = sys.argv[1]
else:
  fileName = "input"

counter = 0
startTime = time.time()
for line in fileinput.input('./'+fileName+'.txt'):
    cleanLine = line.strip("\n")
    segements = cleanLine.split(',')
    seg11,seg12 = segements[0].split('-')
    seg21,seg22 = segements[1].split('-')
    if int(seg11) >= int(seg21) and int(seg11) <= int(seg22):
      counter +=1
    elif int(seg12) >= int(seg21) and int(seg12) <= int(seg22):
      counter +=1
    elif int(seg21) >= int(seg11) and int(seg21) <= int(seg12):
      counter +=1
    elif int(seg22) >= int(seg11) and int(seg22) <= int(seg12):
      counter +=1

print('result: ', counter)
print("--- %s seconds ---" % (time.time() - startTime))